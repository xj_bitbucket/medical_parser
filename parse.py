#!/usr/bin/python
import codecs


class TaggedSentence(object):
    """ Data structure for tagged sentence
    """

    def __init__(self):
        self.intent = None
        self.query = None
        self.tags = []

    def add_tag(self, line):
        sp = line.split('\t')
        assert(len(sp) == 3)
        self.tags.append(sp)
        tag, slot, start_idx = sp
        start_idx = int(start_idx)
        sub_str = self.query[start_idx:start_idx + len(slot)]
        assert(sub_str == slot)

    def get_tags(self):
        return set([x[0] for x in self.tags])


def process(fn):
    fr = codecs.open(fn, 'r')
    in_sen = False
    sentences = []
    sen = None
    all_tags = set()
    all_intents = set()
    for line in fr.xreadlines():
        line = line.strip().decode('utf8')
        print(u"[processing] " + line)

        if not line and not in_sen:
            sen = None
            continue
        elif not line and in_sen:
            in_sen = False
            assert(sen)
            sentences.append(sen)
            all_tags.update(sen.get_tags())
        elif line and not in_sen:
            # new sen found
            sen = TaggedSentence()
            (sen.query, sen.intent) = line.split('\t')
            all_intents.add(sen.intent)
            in_sen = True
        elif line and in_sen:
            sen.add_tag(line)
        else:
            assert(False, "Logic error")

    if sen:
        sentences.append(sen)
        all_tags.update(sen.get_tags())

    print("found {} sentences".format(len(sentences)))
    print(all_tags)
    for intent in all_intents:
        print intent


if __name__ == '__main__':
    import sys
    fn_in = sys.argv[1]
    process(fn_in)

