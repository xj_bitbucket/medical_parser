#! coding:utf-8
import os,sys
import codecs
import jieba
import jieba.posseg
from collections import Counter

def replaceNum(**kwargs):
    sens = kwargs['sens']
    if kwargs['args'] and 'token' in kwargs['args']:
        token = kwargs['args']['token']
    else:
        token = '<NUM>'
    new_sens = []
    for sen in sens:
        new_sen = []
        for w in sen:
            if w.startswith('-'):
                _w = w[1:]
            else:
                _w = w
            if _w.isdigit():
                new_sen.append(token)
            elif _w.count('.') == 1:
                ws = _w.split('.')
                if ws[0].isdigit() and ws[1].isdigit():
                    new_sen.append(token)
                else:
                    new_sen.append(w)
            else:
                new_sen.append(w)
        new_sens.append(new_sen)
    return new_sens

def removeStopWord(**kwargs):
    sens = kwargs['sens']
    word_file = kwargs['args']['wordfile']
    if kwargs['args'] and token in kwargs['args']:
        token = kwargs['args']['token']
    else:
        token = None
    with codecs.open(word_file, 'r', 'utf-8') as f:
        stopwords = [line.strip() for line in f.readlines()]
    new_sens = []
    for sen in sens:
        new_sen = []
        for w in sen:
            if w not in stopwords:
                new_sen.append(w)
            elif token:
                new_sen.append(token)
        new_sens.append(new_sen)
    return new_sens

def removeLastPunc(**kwargs):
    sens = kwargs['sens']
    punctuations = kwargs['args']['puncs']
    for sen in sens:
        if sen[-1] in punctuations:
            sen.pop(-1)
    return sens


def filter_by_rules(sens, rules, args):
    for i, r in enumerate(rules):
        sens = r(sens=sens, args=args[i])
    return sens

def segment_file_sentences(infile, dictfile=None):
    with codecs.open(infile, 'r', 'utf-8') as f:
        lines = f.readlines()
        lines = [line.strip() for line in lines]
    seg_lines = []
    if dictfile:
        jieba.load_userdict(dictfile)
    for line in lines:
        cut = jieba.cut(line)
        res = [w for w in cut]
        seg_lines.append(res)
    return seg_lines

def postag_file_sentences(infile, dictfile=None):
    with codecs.open(infile, 'r', 'utf-8') as f:
        lines = f.readlines()
        lines = [line.strip() for line in lines]
    seg_lines = []
    for line in lines[:10]:
        seged = jieba.posseg.cut(line)
        outstr = ""
        for x in seged:
            outstr +=  u"{}/{} ".format(x.word, x.flag)
        print outstr


def write_sentences(outfile, sens):
    with codecs.open(outfile, 'w', 'utf-8') as f:
        for sen in sens:
            f.write(sen + '\n')

def write_vocabulary(infile, outfile):
    with codecs.open(infile, 'r', 'utf-8') as f:
        lines = [line.strip().split() for line in f.readlines()]
    words = []
    for line in lines:
        words.extend(line)
    count = list(Counter(words).items())
    count = sorted(count, key=lambda x:x[1], reverse=True)
    print len(count)
    for c in count:
        if c[1] > 10:
            print c[0], c[1]

if __name__ == '__main__':
    infile = sys.argv[1]
    # outfile = sys.argv[2]
    # dictfile = sys.argv[3]
    # sens = segment_file_sentences(infile, dictfile)
    # sens = filter_by_rules(sens, [replaceNum, removeLastPunc], [None, {'puncs':[u'？',u'。']}])
    # sens = [' '.join(sen) for sen in sens]
    # write_sentences(outfile, sens)
    # write_vocabulary('../data/data.890.num.txt', '')
    postag_file_sentences(infile)


    