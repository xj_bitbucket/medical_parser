#! coding:utf-8
import codecs

def read_lines(infile):
    with codecs.open(infile, 'r', 'utf-8') as f:
        lines = [line.strip() for line in f.readlines()]
    return lines

def get_slots(lines, tokenlevel=True):
    tags = set()
    sens = []
    slots = []
    for line in lines:
        if line == '':
            continue
        eles = line.split('\t')
        if len(eles) == 1:
            sens.append(list(line))
            slots.append([u'other']*len(sens[-1]))
        elif len(eles) == 3:
            tags.add(eles[0])
            length = len(list(eles[1]))
            sind = int(eles[2])
            if tokenlevel:
                slots[-1][sind:sind + length] = [eles[0]] * length
            else:
                slots[-1][sind] = eles[0] + '-B'
                slots[-1][sind+1:sind+length] = [eles[0] + '-I']*(length-1)
    return sens, slots

def ident_tokens(sens, slots, tokens):
    new_slots = []
    for sen, slot, tok in zip(sens, slots, tokens):
        # print (sen, tok)
        i = j = 0
        while i < len(sen) and j < len(tok):
            if sen[i] == tok[j]:
                i += 1
                j += 1
            else:
                k = 1
                tmp = sen[i]
                while k < len(sen) - i and tmp != tok[j]:
                    tmp += sen[i+k]
                    k += 1
                slot[i+1:i+k] = ['']*(k-1)
                i += k
                j += 1
        new_slots.append([s for s in slot if s != ''])
    return new_slots

if __name__ == '__main__':
    lines = read_lines('../data/slot_mark.txt')
    sens, slots = get_slots(lines, tokenlevel=False)
    k = 0
    for sen, slot in zip(sens, slots):
        k += 1
        for i in zip(sen, slot):
            print(i[0], i[1])
        print()
        if k >= 30:
            break



