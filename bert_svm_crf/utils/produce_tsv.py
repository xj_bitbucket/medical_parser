#! coding:utf-8
import os, sys, codecs
import sklearn
import numpy as np
from sklearn.model_selection import StratifiedKFold

def read_raw(textfile, labelfile):
    with codecs.open(textfile, 'r', 'utf-8') as f:
        text = [line.strip() for line in f.readlines()]
    with codecs.open(labelfile, 'r', 'utf-8') as f:
        label = [line.strip() for line in f.readlines()]
    return np.asarray(text), np.asarray(label)

def write_tsv(data, filename):
    text, label = data
    with codecs.open(filename, 'w', 'utf-8') as f:
        for t, l in zip(text,label):
            f.write(t + u'\t' + l + u'\n')


def split_data(data, dirname, nfold=5):
    text, label = data
    skf = StratifiedKFold(n_splits=nfold, shuffle=True)
    i = 0
    files = ['train.tsv', 'dev.tsv']
    for tr, de in skf.split(text, label):
        lastdir = os.path.join(dirname,'fold%d'%i)
        if not os.path.exists(lastdir):
            os.mkdir(lastdir)
        write_tsv([text[tr], label[tr]], os.path.join(lastdir, files[0]))
        write_tsv([text[de], label[de]], os.path.join(lastdir, files[1]))
        i += 1

if __name__ == '__main__':
    textfile = sys.argv[1]
    labelfile = sys.argv[2]
    outdir = sys.argv[3]
    data = read_raw(textfile, labelfile)
    split_data(data, outdir)
