#! coding:utf-8
import json


def read_lines(infile):
    with open(infile, 'r') as f:
        lines = [line.strip() for line in f.readlines()]
    return lines

def get_features(lines, drophead=True):
    features = []
    tokens = []
    for line in lines:
        obj = json.loads(line)
        jsons = obj['features']
        feats = []
        toks = []
        for f in jsons:
            feats.append(f['layers'][0]['values'])
            toks.append(f['token'])
        if drophead:
            features.append(feats[1:-1])
            tokens.append(toks[1:-1])
        else:
            features.append(feats)
            tokens.append(toks)
    return features, tokens

if __name__ == '__main__':
    lines = read_lines('../data/slot.jsonl')
    get_features(lines[:10])