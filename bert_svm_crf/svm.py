#! coding:utf-8
import os, codecs, json
import sklearn
import numpy as np
from text_feature import unified_feature
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.decomposition import PCA


def get_label(infile):
    with codecs.open(infile, 'r', 'utf-8') as f:
        lines = f.readlines()
        lines = [line.strip() for line in lines]
    tags = list(set(lines))
    lines = [tags.index(line) for line in lines]
    return np.asarray(lines)

def read_feature_from_json(infile):
    with open(infile, 'r') as f:
        lines = [line.strip() for line in f.readlines()]
    feats = []
    for line in lines:
        _obj = json.loads(line)
        # print(_obj['features'][0])
        feats.append(_obj['features'][0]['layers'][0]['values'])
    return np.asarray(feats)

def decomposition(X, num_comp):
    pca = PCA(n_components=num_comp, copy=True, whiten=True)
    pca.fit(X)
    return pca.transform(X), pca

def nfold_test(x, y, fold=3):
    tx = unified_feature()

    clf = sklearn.svm.SVC()
    skf = sklearn.model_selection.StratifiedKFold(n_splits=fold, shuffle=True)
    total_acc = 0.
    logging = []
    for tr, te in skf.split(x,y):
        _x, pca = decomposition(x[tr], 16)
        trx = np.concatenate([_x, tx[tr]], axis=-1)
        clf.fit(trx, y[tr])
        tex = pca.transform(x[te])
        tex = np.concatenate([tex, tx[te]], axis=-1)
        pred_y = clf.predict(tex)
        acc = accuracy_score(y[te], pred_y)
        precision = precision_score(y[te], pred_y)
        recall = recall_score(y[te], pred_y)
        f = f1_score(y[te], pred_y)
        print(len(tr),sum(y[tr]))
        logging.append((te, te[y[te] != pred_y]))
        print(acc)
        total_acc += acc
    return total_acc / fold, logging

def write_record(outfile, lines, raw_labels, logging):
    with codecs.open(outfile,'w','utf-8') as f:
        i = 0
        for al, fal in logging:
            f.write('fold %d\n'%i + '-'*30 + '\n')
            f.write('true sample:\n')
            for ind in set(al) - set(fal):
                f.write(lines[ind].strip() + '\t\t\t' + raw_labels[ind])
            f.write('\nfalse sample:\n')
            for ind in fal:
                f.write(lines[ind].strip() + '\t\t\t' + raw_labels[ind])
            f.write('\n\n')
            i += 1

if __name__ == '__main__':
    y = get_label('./data/sampled_label.txt')
    X = read_feature_from_json('./data/sampled.jsonl')
    # X, _ = decomposition(X, 32)
    # print(X.shape)
    acc, logging = nfold_test(X, y, 5)
    print(acc)
    with codecs.open('./data/sampled_intent.txt','r','utf-8') as f:
        lines = f.readlines()
    with codecs.open('./data/sampled_label.txt','r','utf-8') as f:
        labels = f.readlines()
    write_record('results/record.txt',lines, labels, logging)
