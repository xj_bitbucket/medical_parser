#! coding:utf-8
import codecs
import numpy as np

def get_sentences(infile):
    with codecs.open(infile, 'r', 'utf-8') as f:
        lines = [line.strip() for line in f.readlines()]
    return lines

def find_illness(sens, infile):
    with codecs.open(infile, 'r', 'utf-8') as f:
        lines = [line.strip().split('\t') for line in f.readlines()]
        illness, itype, _ = zip(*lines)
    illness = list(illness)
    itype = list(itype)
    illness[0] = illness[0][1:]
    alltypes = list(set(itype))

    feats = []
    for sen in sens:
        feat = np.zeros(len(alltypes))
        for i, ill in enumerate(illness):
            if ill in sen:
                feat[alltypes.index(itype[i])] += 1
        feats.append(feat)
    return np.asarray(feats)

def find_symptom(sens, infile):
    last = False
    symptoms = []
    feats = []
    with codecs.open(infile, 'r', 'utf-8') as f:
        for line in f.readlines():
            line = line.strip()
            if line.startswith('#'):
                if last:
                    feat = np.zeros(len(sens))
                    for i, sen in enumerate(sens):
                        for s in symptoms:
                            if s in sen:
                                feat[i] += 1
                    feats.append(feat)
                last = True
                symptoms = []
            else:
                smps = line.split('\t')
                symptoms.extend(smps)
    return np.asarray(feats).T

def unified_feature():
    lines = get_sentences('./data/sampled_intent.txt')
    x1 = find_illness(lines, './data/ill_data.txt')
    x2 = find_symptom(lines, './data/symptom_list.txt')
    x = np.concatenate((x1, x2), axis=-1)
    return x


if __name__ == '__main__':
    x = unified_feature()