#! coding:utf-8
import tensorflow as tf
import numpy as np
import os, codecs, json
import sklearn
from sklearn.decomposition import PCA

max_length = 30
tok_dim = 64
cls_dim = 8

def get_label(infile):
    with codecs.open(infile, 'r', 'utf-8') as f:
        lines = f.readlines()
        lines = [line.strip() for line in lines]
    tags = list(set(lines))
    lines = [tags.index(line) for line in lines]
    return np.asarray(lines)

def extract_cls_features(infile):
    with open(infile, 'r') as f:
        lines = [line.strip() for line in f.readlines()]
    feats = []
    for line in lines:
        _obj = json.loads(line)
        # print(_obj['features'][0])
        feats.append(_obj['features'][0]['layers'][0]['values'])
    return np.asarray(feats)

def extract_token_features(infile, maxlen=30):
    with open(infile, 'r') as f:
        lines = [line.strip() for line in f.readlines()]
    feats = []
    length = []
    for line in lines:
        _obj = json.loads(line)
        features = _obj['features']
        tok_feats = []
        for tok in features[1:]:
            tok_feats.append(tok['layers'][0]['values'])
        length.append(len(tok_feats))

        padlen = maxlen - len(tok_feats)
        tok_feats = np.pad(np.asarray(tok_feats), ((0,padlen),(0,0)), 'constant')
        feats.append(tok_feats)
    return np.asarray(feats), np.asarray(length)

def decomposition(X, num_comp):
    pca = PCA(n_components=num_comp, copy=True, whiten=True)
    pca.fit(X)
    return pca.transform(X), pca

def token_decomposition(x, length, num_comp):
    b, l, n = x.shape
    x = np.reshape(x, [-1, n])
    pca = PCA(n_components=num_comp, copy=True, whiten=True)
    pca.fit(x)
    x = pca.transform(x)
    x = np.reshape(x, [-1, l, num_comp])
    new_x = []
    for i in range(b):
        tmp = x[i][:length[i]]
        tmp = np.pad(tmp, ((0,l-length[i]),(0,0)), 'constant')
        new_x.append(tmp)
    return np.asarray(new_x), pca

def cnn(scope, x, feats, de, uni_num, tri_num, five_num, dropout, maxlen=30):
    with tf.variable_scope(scope):
        feats = tf.expand_dims(feats, axis=1)
        unifilter = tf.get_variable('unigram', [1, de, uni_num])
        trifilter = tf.get_variable('trigram', [2, de, tri_num])
        fivefilter = tf.get_variable('fivegram', [3, de, five_num])
        b = tf.get_variable('b', [uni_num+tri_num+five_num])
        uniconv1d = tf.nn.conv1d(x, unifilter, 1, 'SAME', )
        triconv1d = tf.nn.conv1d(x, trifilter, 1, 'SAME')
        fiveconv1d = tf.nn.conv1d(x, fivefilter, 1, 'SAME')
        conv1d = tf.concat([uniconv1d, triconv1d, fiveconv1d], axis=-1)
        added = conv1d + b
        added = tf.nn.dropout(added, keep_prob=1-dropout)
        pooled = tf.layers.max_pooling1d(conv1d, maxlen, 1)
        combined = tf.concat([feats, pooled], axis=-1)
        logits = tf.layers.dense(combined, 1, activation=tf.nn.sigmoid)
        return tf.squeeze(logits)

def n_fold_test(x, cls_x, y, nfold=3):
    clf = sklearn.svm.SVC()
    skf = sklearn.model_selection.StratifiedKFold(n_splits=nfold, shuffle=True)
    total_acc = 0.
    logging = []
    i = 0
    for tr, te in skf.split(x, y):
        acc = test_and_eval(x[tr], cls_x[tr], y[tr], x[te], cls_x[te], y[te], i)
        print '=====>,', acc
        total_acc += acc
        i += 1
    print total_acc / nfold


def test_and_eval(x_train, cls_train, y_train, x_test, cls_test, y_test, round):

    _x = tf.placeholder(tf.float32, [None, max_length, tok_dim])
    _cls = tf.placeholder(tf.float32, [None, cls_dim])
    _y = tf.placeholder(tf.float32, [None])
    _dp = tf.placeholder(tf.float32, [])

    logits = cnn('round%d'%round, _x, _cls, tok_dim, 8, 8, 8, _dp, max_length)
    loss = tf.reduce_mean(-_y * tf.log(tf.clip_by_value(logits, 1e-10, 1.0)) - (1 - _y) * tf.log(
        tf.clip_by_value(1. - logits, 1e-10, 1.0)))
    optimizer = tf.train.AdamOptimizer(0.01)
    train_op = optimizer.minimize(loss)
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        print round
        for i in range(3000):
            _, prob, ls = sess.run([train_op, logits, loss], feed_dict={_x: x_train, _cls:cls_train, _y: y_train, _dp: 0.2})
            if (i+1)%500 == 0:
                print ls
        pred = sess.run(logits, feed_dict={_x: x_test, _cls:cls_test, _dp: 0.})
        acc = np.sum(np.int32(pred >= 0.5) == y_test) / float(len(y_test))
        return acc



if __name__ == '__main__':
    y = get_label('./data/sampled_label.txt')
    x, length = extract_token_features('./data/sampled.jsonl')
    cls_x = extract_cls_features('./data/sampled.jsonl')
    x, _ = token_decomposition(x, length, tok_dim)
    cls_x, _ = decomposition(cls_x, cls_dim)
    n_fold_test(x, cls_x, y)



