#! coding:utf-8
import sys
import codecs
from collections import Counter

def read_senfile(infile):
    with codecs.open(infile, 'r', 'utf-8') as f:
        lines = [line.strip().split() for line in f.readlines()]
    return lines

def ngram(sens, n=3):
    prefix = ['@'] * (n-1)
    post = ['@']
    bag = []
    for sen in sens:
        _sen = prefix + sen + post
        i = 0
        while i <= len(_sen) - n:
            bag.append(' '.join(_sen[i:i+n]))
            i += 1
    res = Counter(bag).most_common(100)
    return res

def tf_idf(sens):
    word_bag = set()
    for sen in sens:
        for w in sen:
            word_bag.add(w)
    tf = dict.

if __name__ == '__main__':
    infile = sys.argv[1]
    sens = read_senfile(infile)
    res = ngram(sens, n=2)
    for r in res:
        print r[1], r[0]
