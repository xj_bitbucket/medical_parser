#! coding:utf-8
import os,sys
import numpy as np
import tensorflow as tf
from sklearn.decomposition import PCA
from sklearn.model_selection import KFold

from utils import Features, Labels

feature_size = 128
n_tags = 13
batch_size = 32
learning_rate = 1e-2

def get_chunks(labels, label2id):
    id2label = dict([(x[1],x[0]) for x in label2id.items()])
    batch_slots = []
    for label in labels:
        slots = []
        for i,id in enumerate(label):
            if id == 0:
                continue
            elif id % 2 == 1:
                slot = (id2label[id].split('-')[0], i, 1)
                slots.append(slot)
            else:
                _slot = slots.pop()
                slot = (_slot[0], _slot[1], _slot[2]+1)
                slots.append(slot)
        batch_slots.append(slots)
    return batch_slots

def metric(true_slots, pred_slots):
    correct_preds = 0.
    total_preds = 0.
    total_correct = 0.
    for true, pred in zip(true_slots, pred_slots):
        correct_preds += len(set(true) & set(pred))
        total_preds += len(set(pred))
        total_correct += len(set(true))

    p = correct_preds / total_preds if correct_preds > 0 else 0
    r = correct_preds / total_correct if correct_preds > 0 else 0
    f1 = 2 * p * r / (p + r) if correct_preds > 0 else 0
    print p, r, f1
    return p, r, f1

def metric_by_tokens(true_labels, pred_labels):
    true_labels[true_labels == 0] = -1
    correct_preds = np.sum(true_labels == pred_labels)
    total_preds = np.sum(pred_labels != 0)
    total_correct = np.sum(true_labels != -1)

    p = correct_preds / total_preds if correct_preds > 0 else 0
    r = correct_preds / total_correct if correct_preds > 0 else 0
    f1 = 2 * p * r / (p + r) if correct_preds > 0 else 0
    print p, r, f1
    return p, r, f1


def prepare_features(infile, maxlen=25):
    lines = Features.read_lines(infile)
    feats, tokens = Features.get_features(lines)
    features = []
    length = []
    for f in feats:
        f = np.asarray(f)
        l = len(f)
        padded = np.pad(f, ((0,maxlen-l), (0,0)), 'constant')
        features.append(padded)
        length.append(l)
    return np.asarray(features), np.asarray(length), tokens

def prepare_labels(infile, tokens, maxlen=25):
    lines = Labels.read_lines(infile)
    sens, slots = Labels.get_slots(lines)
    slots = Labels.ident_tokens(sens, slots, tokens)
    tags = set()
    labels = []
    for slot in slots:
        tags.update(slot)
    tags.remove('other')
    tags2id = {'other':0}
    for i, l in enumerate(sorted(tags)):
        tags2id[l] = i + 1
    for slot in slots:
        label = [tags2id[s] for s in slot]
        label = np.pad(np.asarray(label),(0,maxlen-len(label)), 'constant')
        labels.append(label)
    return np.asarray(labels), tags2id

def decomposition(features, length, numcomp):
    pca = PCA(n_components=numcomp, copy=True, whiten=True)
    lines, lens, feats = features.shape
    tmp = []
    for i,f in enumerate(features):
        tmp.extend(f[:length[i]])
    pca.fit(tmp)
    features = pca.transform(np.reshape(features, [-1, feats]))
    return np.reshape(features, [lines, lens, numcomp])


def crf_proj(scope, features, hidden_size, num_tags):
    batch_size = tf.shape(features)[0]
    maxlen = tf.shape(features)[1]
    with tf.variable_scope(scope):
        W = tf.get_variable('W', dtype=tf.float32,
                shape=[hidden_size, num_tags])

        b = tf.get_variable('b', dtype=tf.float32,
                shape=[num_tags], initializer=tf.zeros_initializer())

    reshaped_feats = tf.reshape(features, [-1, hidden_size])
    logits = tf.matmul(reshaped_feats, W) + b
    logits = tf.reshape(logits, [batch_size, maxlen, num_tags])
    return logits

def k_fold_test(n_split=3):
    feats, lens, tokens = prepare_features('./data/output.jsonl')
    feats = decomposition(feats, lens, feature_size)
    labels, label2id = prepare_labels('./data/slot_mark.txt', tokens)

    kf = KFold(shuffle=True)
    i = 1
    for _train, _test in kf.split(feats):
        train = feats[_train], labels[_train], lens[_train]
        test = feats[_test], labels[_test], lens[_test]
        preds = train_and_evaluate(train, test, 'round%d'%i)
        true_slots = get_chunks(labels[_test], label2id)
        pred_slots = get_chunks(preds, label2id)
        metric(true_slots, pred_slots)
        # metric_by_tokens(labels[_test], preds)
        i += 1



def train_and_evaluate(train, test, scope):
    _features = tf.placeholder(tf.float32, shape=[None, None, feature_size])
    _labels = tf.placeholder(tf.int32, shape=[None, None])
    _lengths = tf.placeholder(tf.int32, shape=[None])
    _lr = tf.placeholder(tf.float32)

    with tf.variable_scope(scope):
        logits = crf_proj(scope, _features, feature_size, n_tags)
        loglikeli, trans_params = tf.contrib.crf.crf_log_likelihood(logits, _labels, _lengths)
        loss = tf.reduce_mean(-loglikeli)
        optimizer = tf.train.AdamOptimizer(_lr)
        train_op = optimizer.minimize(loss)

        decode_tags, scores = tf.contrib.crf.crf_decode(logits, trans_params, _lengths)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        train_features, train_labels, train_lengths = train

        indexes = np.arange(len(train_features))
        lr = learning_rate
        ls_record = []
        for i in range(4000):
            choosen = np.random.choice(indexes, batch_size, replace=False)
            _, ls = sess.run([train_op, loss], feed_dict={_features:train_features[choosen], _lr:lr,
                                                          _labels:train_labels[choosen], _lengths:train_lengths[choosen]})
            if len(ls_record) > 0 and ls > max(ls_record[-100:]) and lr > 5e-3:
                lr *= 0.9
            ls_record.append(ls)
            if (i+1) % 100 == 0:
                print 'iter %d: ==> %f'%(i+1, ls)

        test_features, test_labels, test_lengths = test

        pred_labels = sess.run(decode_tags, feed_dict={_features:test_features, _lengths:test_lengths})

        return pred_labels


if __name__ == '__main__':
    # feats, lens, tokens = prepare_features('./data/output.jsonl')
    # feats = decomposition(feats, lens, feature_size)
    # labels, _ = prepare_labels('./data/slot_mark.txt', tokens)
    # batch_slots = get_chunks(labels[:5], _)
    k_fold_test()
