## Stats

1. 968 数据, 6 种slot, 5种intent
2. slot 词典缺失: 除了ill, 其他的都缺失


## Plan

- 运行BERT CRF代码，拿到benchmark
- 测试CRF++
- 加入字典Feature
- 尝试收集更多的数据
- Slot-level f1 score > 0.9
